FROM python:3.12 AS builder

WORKDIR /app

COPY . /app

RUN pip install poetry && \
    poetry self add "poetry-dynamic-versioning[plugin]" && \
    poetry install --without dev && \
    poetry build

FROM alpine:3.20
LABEL org.opencontainers.image.source="https://gitlab.com/atomicptr/hoyo-daily-logins-helper"

WORKDIR /app

RUN apk add --no-cache python3 pipx

COPY --from=builder /app/dist /app/dist

RUN python -m pipx install dist/hoyo_daily_logins_helper-*.whl

RUN ln -s /root/.local/bin/hoyo-daily-logins-helper /usr/bin/hoyo-daily-logins-helper

CMD ["hoyo-daily-logins-helper"]
